<?php


namespace App\Controller;


use App\Entity\Task;
use App\Form\TaskCreateType;
use App\Form\TaskEditType;
use App\Repository\TaskRepository;
use App\Serializer\FormErrorSerializer;
use App\Serializer\TaskSerializer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ApiController extends AbstractController
{

    /**
     * @var TaskRepository
     */
    private $taskRepository;

    public function __construct(TaskRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    public function index(TaskSerializer $taskSerializer): Response
    {
        $tasks = $this->taskRepository->findAll();
        return new JsonResponse(array_map(function (Task $task) use ($taskSerializer) {
            return $taskSerializer->serialize($task);
        }, $tasks));
    }

    public function new(Request $request, FormErrorSerializer $formErrorSerializer): Response
    {
        $data = json_decode(
            $request->getContent(),
            true
        );
        $task = new Task();
        $form = $this->createForm(TaskCreateType::class, $task);
        $form->submit($data);
        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->persist($task);
            $this->getDoctrine()->getManager()->flush();
            return new JsonResponse([
                'status' => 'success'
            ], JsonResponse::HTTP_CREATED, [
                'Location' => $this->generateUrl('api_show', [
                    'id' => $task->getId()
                ], UrlGeneratorInterface::ABSOLUTE_URL)
            ]);
        }
        return new JsonResponse([
            'status' => 'error',
            'errors' => $formErrorSerializer->serialize($form)
        ], JsonResponse::HTTP_BAD_REQUEST);
    }

    public function show(TaskSerializer $taskSerializer, $id): Response
    {
        $task = $this->findTask($id);
        if ($task !== null) {
            return new JsonResponse($taskSerializer->serialize($task));
        }
        return new JsonResponse(null, JsonResponse::HTTP_NOT_FOUND);
    }


    public function edit(Request $request, FormErrorSerializer $formErrorSerializer, $id): Response
    {
        $task = $this->findTask($id);
        if ($task === null) {
            return new JsonResponse([
                'status' => 'error',
                'errors' => ["Task with id=$id not found"]
            ], JsonResponse::HTTP_NOT_FOUND);
        }
        $data = json_decode(
            $request->getContent(),
            true
        );
        //fix: symfony has some troubles with parsing boolean values(false_values not working correct)
        if (array_key_exists('is_done', $data) && !is_bool($isDone = $data['is_done'])) {
            if (is_numeric($isDone)) {
                $data['is_done'] = (bool)(int)$isDone;
            } elseif (is_string($isDone)) {
                $data['is_done'] = !in_array(strtolower(trim($isDone)), ['false', '']);
            } else {
                $data['is_done'] = false;
            }
        }
        $form = $this->createForm(TaskEditType::class, $task);
        $form->submit($data, false);
        if ($form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return new JsonResponse(['status' => 'success']);
//            return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT); or we can return this
        }
        return new JsonResponse([
            'status' => 'error',
            'errors' => $formErrorSerializer->serialize($form)
        ], JsonResponse::HTTP_BAD_REQUEST);
    }

    public function delete($id): Response
    {
        $task = $this->findTask($id);
        if ($task === null) {
            return new JsonResponse([
                'status' => 'error',
                'errors' => ["Task with id=$id not found"]
            ], JsonResponse::HTTP_NOT_FOUND);
        }
        $this->getDoctrine()->getManager()->remove($task);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(['status' => 'success']);
    }

    private function findTask($id)
    {
        return $this->taskRepository->find($id);
    }
}