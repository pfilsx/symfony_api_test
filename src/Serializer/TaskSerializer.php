<?php


namespace App\Serializer;


use App\Entity\Task;
use Exception;
use Symfony\Component\Routing\RouterInterface;

class TaskSerializer implements SerializerInterface
{
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param Task $task
     * @return array
     */
    public function serialize($task): array
    {
        if (!$task instanceof Task){
            throw new Exception('TaskSerializer works only with Task entities');
        }
        return [
            'url' => $this->router->generate('api_show', ['id' => $task->getId()], RouterInterface::ABSOLUTE_URL),
            'attributes' => [
                'title' => $task->getTitle(),
                'isDone' => $task->getIsDone()
            ]
        ];
    }
}