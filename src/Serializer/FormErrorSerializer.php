<?php


namespace App\Serializer;


use Exception;
use Symfony\Component\Form\FormInterface;

class FormErrorSerializer implements SerializerInterface
{
    /**
     * @param FormInterface $data
     * @return array
     */
    public function serialize($data): array
    {
        if (!$data instanceof FormInterface) {
            throw new Exception('FormErrorSerializer works only with FormInterface');
        }

        $form = $errors = [];

        foreach ($data->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }

        if ($errors) {
            $form['errors'] = $errors;
        }

        $children = [];
        foreach ($data->all() as $child) {
            if ($child instanceof FormInterface) {
                $errors = $this->serialize($child);
                if (!empty($errors)){
                    $children[$child->getName()] = $errors;
                }
            }
        }

        if ($children) {
            $form['fields'] = $children;
        }

        return $form;
    }
}
